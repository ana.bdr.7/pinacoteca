<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Obra;

class Artista extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Busca la relacion entre artista y obras
     */

    public function obras(){
        return $this->hasMany(Obra::class);
    }

    /**
     * une los generos que contiene artistas
     * 
     */

    public function generos(){
        return $this->belongsTo(Genero::class,'genero1')->get()
            ->merge($this->belongsTo(Genero::class,'genero2')->get())
                ->merge($this->belongsTo(Genero::class,'genero3')->get());
    }
}
