<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function artistas(){
        return $this->hasMany(Artista::class,'genero1')->orWhere('genero2')->orWhere('genero3',$this->id);
    }
}
