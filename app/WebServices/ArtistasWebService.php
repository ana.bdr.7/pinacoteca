<?php

namespace App\WebServices;

use Illuminate\Support\Facades\DB;
use App\Models\Artista;

/**
 * Description of Zoologico
 * 
 * @author ana
 */

class ArtistasWebService {



    /**
     * Devuelve el id de un artista
     * 
     * @param integer $artista_id
     * @return integer
     */
    public function getNumeroObrasArtista($artista_id){
        $artista = Artista::find($artista_id);
        $obras = count($artista->obras);
        return $obras;
        
    }

    /**
     * Busca las obras de un artista
     * @param integer $artista_id
     * @return array
     */
    public function getObrasArtista($artista_id){
        $obras =  DB::table('obras')
        ->where('artista_id','=',$artista_id)
        ->get();
        return $obras;
    }

   
}
?>