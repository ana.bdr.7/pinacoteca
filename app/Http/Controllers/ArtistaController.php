<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artista;
use Illuminate\Support\Facades\DB;

class ArtistaController extends Controller
{
    public function inicio(){
        return redirect()->action([ArtistaController::class,'index']);
    }

    public function index(){

        $artistas = Artista::all();

       

        return view('artistas.index',['artistas'=>$artistas]);
    }

    public function show(Artista $artista){

        
        
        return view('artistas.show',['artista' => $artista]);
    }

    public function buscar(Request $request){
        
         $artista = DB::table('artistas')->where('nombre','like','%'.$request->artista.'%')->get();
         
         return response()->json($artista);
         
     }
}
