<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Obra;

class GeneroController extends Controller
{
    public function show($obra_id){
        $obra = Obra::find($obra_id);
        
        $generos = $obra->artista->generos();
        
        
        return view('generos.show',['generos'=>$generos]);
    }
}
