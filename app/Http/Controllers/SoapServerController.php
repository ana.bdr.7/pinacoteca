<?php

namespace App\Http\Controllers;

use SoapServer;
use App\WebServices\ArtistasWebService;
use App\WebServices\WSDLDocumentException;
use App\WebServices\WSDLDocument;

class SoapServerController extends Controller
{
    private $clase = ArtistasWebService::class;
    private $uri = "http://pinacoteca.local/api";
    private $uriWSDL = "http://pinacoteca.local/api/wsdl";

    public function getServer(){
        
        $server = new SoapServer($this->uriWSDL);
        $server->setClass($this->clase);
        $server->handle();
        exit;
    }

    public function getWSDL(){
        $wsdl = new WSDLDocument($this->clase,$this->uri,$this->uri);
        $wsdl->formatOutput = true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit;
    }
}
