<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artista;
use App\Models\Obra;


class ObraController extends Controller
{
    public function create(){
        $artistas = Artista::all();

        return view('obras.create',['artistas' => $artistas]);
    }

    public function store(Request $request){
        dd($request);
        //$datos = $request->except('_token');
        $datos['nombre'] = $request->nombre;
        $datos['imagen'] = $request->imagen->store('','obras');
        $datos['artista_id'] = $request->artista;
        

        
        try{
            $obra = Obra::create($datos);
            return redirect()->route('artistas.index')->with('mensaje','Animal subido correctamente');
        }catch(Illuminate\Database\QueryException $ex){            
            return redirect()->route('artistas.index')->with('mensaje','Fallo al subir el animal');
        }
            
       
    }

}
