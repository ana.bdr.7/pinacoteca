
@extends('layouts.master') 
 
 @section('titulo')
   Artista
 @endsection 
  

 
 
 @section('contenido')
 <div class="container">
     <div class="row">
     <table>
     <tr><th>Nombre</th><th>Pais</th><th>Obras<tH></tr>
         @foreach($artistas as $clave => $artista)
         <tr>
            <td><a href="{{ route('artistas.show', $artista ) }}">{{$artista->nombre}}</a></td>
            <td>{{$artista->pais}}</td>
            <td>{{count($artista->obras)}}</td>
         </tr>
             
         @endforeach
         </table>
      </div>
 
   
 </div>
 @endsection 