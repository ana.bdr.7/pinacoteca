@extends('layouts.master')

@section('titulo')
    Artista
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 
    
    <div class="col-sm-9"> 
    
       <h2><strong>{{$artista->nombre}}</strong></h2>
       <p><strong>{{$artista->pais}}</strong></p>
       <p><strong>{{$artista->fechaNacimiento}}</strong></p>
      
        @foreach($artista->obras as $obra)
        <div class="col-sm-3">
        
        <a href="{{ route('generos.show', $obra->id) }}">{{$obra->nombre}}</a>     
            <img class="imagen" src="{{asset('/assets/img/' .$obra->imagen)}}">
        </div> 
    @endforeach
       
      
   
 
      
    </div> 
</div> 
 
@endsection