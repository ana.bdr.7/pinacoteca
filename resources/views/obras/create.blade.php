@extends('layouts.master') 
 
@section('titulo')
 Vinacoteca 
@endsection 
 
@section('contenido')
    @if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
  <div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center">             
                 Añadir Obra         
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action= "{{ route('obras.store') }}" enctype="multipart/form-data">             
                    @csrf       
                    
                    <div class="form-group">                 
                        <label for="titulo">Artistas</label>
                        <select id="artista" name="artista">
                        @foreach($artistas as $artista)
                        <option value="{{$artista->id}}">{{$artista->nombre}}</option>
                        @endforeach 
                        </select>                
                        
                    </div>  

                    <div class="form-group">                 
                        <label for="titulo">Artista Autocomplete</label>                 
                        <input type="text" name="artista2" id="artista2" class="form-control" required>  
                        <input type="hidden" name="artista2_id" id="artista2_id" class="form-control">              
                    </div>  
                           
                    <div class="form-group">                 
                        <label for="titulo">Titulo</label>                 
                        <input type="text" name="nombre" id="nombre" class="form-control" required>              
                    </div>                    
                    

                    <div class="form-group"> 
                        <label for="imagen">Imagen</label>                                         
                        <input type="file" name="imagen" id="imagen" class="form-control" required>             
                    </div> 

                    <div class="form-group text-center">                
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">             
                        Añadir Imagen              
                    </button>              
                    </div>              
                </form>                      
            </div>       
        </div>     

    </div>  
</div>
<script>
  $(function() {
 
    $( "#artista2" ).autocomplete({
      source: function( request, response ) {

        $.ajax({
          url: "/busquedaAjax",
          dataType: "json",
          method: "post",
          data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            artista: request.term
          },
          success: function( data ) {
              console.log(data)
            response( 
              data.map(m => {
                return {
                  label: m.nombre,
                  id: m.id
                }
              })
            );
          }
        })

      },
      minLength: 1,
      select: function(ui, item){
          document.querySelector('#artista2_id').value = item.item.id
        //location.href = '/obras/crear/'+item.item.value
      }
    } );
  } );
</script>
  
@endsection 

