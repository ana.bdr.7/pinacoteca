<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artistas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('pais');
            $table->date('fechaNacimiento');
            $table->integer('genero1')->unsigned();
            $table->foreign('genero1')->references('id')->on('generos');
            $table->integer('genero2')->unsigned();
            $table->foreign('genero2')->references('id')->on('generos');
            $table->integer('genero3')->unsigned();
            $table->foreign('genero3')->references('id')->on('generos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artistas');
    }
}
