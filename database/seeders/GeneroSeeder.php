<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Genero;
use Illuminate\Support\Str;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $generos = array(
        array(
            'nombre' => 'cubista',            
        ),
        array(
            'nombre' => 'retrato',            
        ),
        array(
            'nombre' => 'realismo',            
        ),
        array(
            'nombre' => 'impresionismo',            
        ),
        array(
            'nombre' => 'surrealismo',            
        ),
    );
    public function run()
    {
        foreach($this->generos as $genero){
            $a = new Genero();
            $a->nombre = $genero['nombre'];
            $a->slug = Str::slug($genero['nombre']);
            $a->save();
        }
        $this->command->info('tabla generos inicializada');
    }
}
