<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\ArtistaSeeder;
use Database\Seeders\GeneroSeeder;
use Database\Seeders\ObraSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('obras')->truncate();
        DB::table('artistas')->truncate();
        DB::table('generos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('generos')->delete();
        $this->call(GeneroSeeder::class);
        DB::table('artistas')->delete();
        $this->call(ArtistaSeeder::class);
        DB::table('obras')->delete();
        $this->call(ObraSeeder::class);


    }
}
