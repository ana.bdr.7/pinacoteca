<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Artista;

class ArtistaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     private $artistas = array(
         array(
             'nombre' => 'Velazquez',
             'pais' => 'España',
             'fechaNacimiento' => '1599-02-01'
         ),
         array(
            'nombre' => 'Picasso',
            'pais' => 'España',
            'fechaNacimiento' => '1881-10-25'
        ),
        array(
            'nombre' => 'Rubens',
            'pais' => 'Alemania',
            'fechaNacimiento' => '1577-06-28'
        ),
        array(
            'nombre' => 'Tiziano',
            'pais' => 'Italia',
            'fechaNacimiento' => '1490-09-11'
        )
    );
    public function run()
    {
        foreach($this->artistas as $artista){
            $a = new Artista();
            $a->nombre = $artista['nombre'];
            $a->pais = $artista['pais'];
            $a->fechaNacimiento = $artista['fechaNacimiento'];
            $a->genero1 = rand(1,5);
            $a->genero2 = rand(1,5);
            $a->genero3 = rand(1,5);
            $a->save();
        }
        $this->command->info('tabla artistas inicializada');
    }
}
