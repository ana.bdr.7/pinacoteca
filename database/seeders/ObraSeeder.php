<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Obra;

class ObraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     protected $obras = array(
         array(
             'nombre' => 'Las Meninas',
             'imagen' => 'meninas.jpg',             
         ),
         array(
            'nombre' => 'Gernika',
            'imagen' => 'gernika.jpg',             
        ),
        array(
            'nombre' => 'La mujer que llora',
            'imagen' => 'lamujerquellora.jpg',             
        ),
        array(
            'nombre' => 'Las 3 Gracias',
            'imagen' => 'las3gracias.jpg',             
        ),
        array(
            'nombre' => 'Baco y Ariadna',
            'imagen' => 'bacoyariadna.jpg',             
        ),
        array(
            'nombre' => 'Carlos V a caballo',
            'imagen' => 'carlosV.jpg',             
        ),
        array(
            'nombre' => 'La Rendicion de Breda',
            'imagen' => 'rendicion.jpg',             
        ),
    );
    public function run()
    {
        foreach($this->obras as $obra){
            $a = new Obra();
            $a->nombre = $obra['nombre'];
            $a->imagen = $obra['imagen'];
            $a->artista_id = rand(1,4);
            $a->save();
        }
        $this->command->info('Tabla obras inicializada');
    }
}
