<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArtistaController;
use App\Http\Controllers\GeneroController;
use App\Http\Controllers\ObraController;
use App\Http\Controllers\SoapServerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/',[ArtistaController::class,'inicio'])->name('artistas.inicio');

Route::get('/artistas',[ArtistaController::class,'index'])->name('artistas.index');

Route::post('/busquedaAjax',[ArtistaController::class,'buscar'])->name('artistas.buscar');

Route::get('/artistas/ver/{artista}',[ArtistaController::class,'show'])->name('artistas.show');

Route::get('genero/{genero}',[GeneroController::class,'show'])->name('generos.show');

Route::get('obras/crear',[ObraController::class,'create'])->name('obras.create');

Route::post('/artistas',[ObraController::class,'store'])->name('obras.store');

Route::any('/api',[SoapServerController::class,'getServer']);

Route::any('api/wsdl',[SoapServerController::class,'getWSDL']);

